package com.gitlab.forcedinduction.gradle.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.MavenPlugin
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin

final class SharedConfigPlugin implements Plugin<Project>
{

    @Override
    void apply(Project project)
    {
        project.repositories {
            mavenLocal()
            mavenCentral()
        }

        project.group = 'gradle.shared.config'

        project.plugins.apply JavaPlugin
        project.properties.sourceCompatibility = 1.8

        project.plugins.apply MavenPlugin

        project.plugins.apply MavenPublishPlugin
        project.extensions.configure(PublishingExtension) { ext ->
            ext.publications {
                jar(MavenPublication) {
                    from project.components.java
                }
            }
            ext.repositories {
                maven { url "file://$project.buildDir/repo" }
            }
        }

        project.tasks.getByName('publish').dependsOn 'build'

        project.task('exampleCustomTask') {
            group = 'Custom'

            doLast {
                println "I'm a custom task!"
            }
        }
    }
}
